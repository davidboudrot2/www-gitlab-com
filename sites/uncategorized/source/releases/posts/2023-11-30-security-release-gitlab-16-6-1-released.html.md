---
title: "GitLab Security Release: 16.6.1, 16.5.3, 16.4.3"
categories: releases
author: Greg Myers
author_gitlab: greg
author_twitter: gitlab
description: "Learn more about GitLab Security Release: 16.6.1, 16.5.3, 16.4.3 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2023/11/30/security-release-gitlab-16-6-1-released.html.md'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---


Today we are releasing versions 16.6.1, 16.5.3, 16.4.3 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases patches for vulnerabilities in dedicated security releases. There are two types of security releases:
a monthly, scheduled security release, released a week after the feature release (which deploys on the 3rd Thursday of each month),
and ad-hoc security releases for critical vulnerabilities. For more information, you can visit our [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of our regular and security release blog posts [here](/releases/categories/releases/).
In addition, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest security release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Table of fixes

| Title | Severity |
| ----- | -------- |
| [XSS and ReDoS in Markdown via Banzai pipeline of Jira](#xss-and-redos-in-markdown-via-banzai-pipeline-of-jira) | High |
| [Members with admin_group_member custom permission can add members with higher role](#members-with-admin_group_member-custom-permission-can-add-members-with-higher-role) | High |
| [Release Description visible in public projects despite release set as project members only through atom response](#release-description-visible-in-public-projects-despite-release-set-as-project-members-only-through-atom-response) | Medium |
| [Manipulate the repository content in the UI (CVE-2023-3401 bypass)](#manipulate-the-repository-content-in-the-ui-cve-2023-3401-bypass) | Medium |
| [External user can abuse policy bot to gain access to internal projects](#external-user-can-abuse-policy-bot-to-gain-access-to-internal-projects) | Medium |
| [Developers can update pipeline schedules to use protected branches even if they don't have permission to merge](#developers-can-update-pipeline-schedules-to-use-protected-branches-even-if-they-dont-have-permission-to-merge) | Medium |
| [Users can install Composer packages from public projects even when `Package registry` is turned off](#users-can-install-composer-packages-from-public-projects-even-when-package-registry-is-turned-off) | Medium |
| [Client-side DOS via Mermaid Flowchart](#client-side-dos-via-mermaid-flowchart) | Low |
| [Unauthorized member can gain `Allowed to push and merge` access and affect integrity of protected branches](#unauthorized-member-can-gain-allowed-to-push-and-merge-access-and-affect-integrity-of-protected-branches) | Low |
| [Guest users can react (emojis) on confidential work items which they cant see in a project](#guest-users-can-react-emojis-on-confidential-work-items-which-they-cant-see-in-a-project) | Low |

### XSS and ReDoS in Markdown via Banzai pipeline of Jira

Improper neutralization of input in Jira integration configuration in GitLab CE/EE, affecting all versions from 15.10 prior to 16.6.1, 16.5 prior to 16.5.3, and 16.4 prior to 16.4.3 allowed attacker to execute javascript in victim's browser.

This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N`, 8.7).
It is now mitigated in the latest release and is assigned [CVE-2023-6033](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6033).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.

### Members with admin_group_member custom permission can add members with higher role

An issue has been discovered in GitLab EE affecting all versions starting from 16.5 before 16.5.3, 
all versions starting from 16.6 before 16.6.1. When a user is assigned a custom role with `admin_group_member`` enabled, they may be able to add a member with a higher static role than themselves to the group which may lead to privilege escalation.

This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:N`, 8.1).
It is now mitigated in the latest release and is assigned [CVE-2023-6396](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6396).

This vulnerability was discovered internally by GitLab team member [jarka](https://gitlab.com/jarka).

### Release Description visible in public projects despite release set as project members only through atom response

An issue has been discovered in GitLab affecting all versions starting from 11.3 before 16.4.3, all versions starting from 16.5 before 16.5.3, all versions starting from 16.6 before 16.6.1.
It was possible for unauthorized users to view a public projects' release descriptions via an atom endpoint when release access on the public was set to only project members

This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N`, 5.3).
It is now mitigated in the latest release and is assigned [CVE-2023-3949](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3949).

Thanks [ashish_r_padelkar](https://hackerone.com/ashish_r_padelkar) for reporting this vulnerability through our HackerOne bug bounty program.

### Manipulate the repository content in the UI (CVE-2023-3401 bypass)

An issue has been discovered in GitLab affecting all versions before 16.4.3, all versions starting from 16.5 before 16.5.3, all versions starting from 16.6 before 16.6.1. Under certain circumstances, a malicious actor bypass prohibited branch checks using a specially crafted branch name to manipulate repository content in the UI.

This is a medium severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:N/I:H/A:N`, 4.8).
It is now mitigated in the latest release and is assigned [CVE-2023-5226](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5226).

Thanks [shells3c](https://hackerone.com/shells3c) for reporting this vulnerability through our HackerOne bug bounty program.

### External user can abuse policy bot to gain access to internal projects

An issue has been discovered in GitLab EE affecting all versions starting from 16.2 before 16.4.3, all versions starting from 16.5 before 16.5.3, all versions starting from 16.6 before 16.6.1. It was possible for an attacker to abuse the policy bot to gain access to internal projects.

This is a medium severity issue (`CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:U/C:H/I:N/A:N`, 4.4). It is now mitigated in the latest release and is assigned [CVE-2023-5995](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5995).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.

### Client-side DOS via Mermaid Flowchart

An issue has been discovered in GitLab EE affecting all versions starting from 10.5 before 16.4.3, all versions starting from 16.5 before 16.5.3, all versions starting from 16.6 before 16.6.1. It was possible for an attacker to cause a client-side denial of service using malicious crafted mermaid diagram input.

This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:N/I:N/A:L`, 2.6).
It is now mitigated in the latest release and is assigned [CVE-2023-4912](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4912).

Thanks [toukakirishima](https://hackerone.com/toukakirishima) for reporting this vulnerability through our HackerOne bug bounty program.

### Developers can update pipeline schedules to use protected branches even if they don't have permission to merge

An issue has been discovered in GitLab affecting all versions starting from 9.2 before 16.4.3, all versions starting from 16.5 before 16.5.3, all versions starting from 16.6 before 16.6.1. It was possible for a user with the Developer role to update a pipeline schedule from an unprotected branch to a protected branch.

This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N`, 4.3).
It is now mitigated in the latest release and is assigned [CVE-2023-4317](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4317).

Thanks [js_noob](https://hackerone.com/js_noob) for reporting this vulnerability through our HackerOne bug bounty program.

### Users can install Composer packages from public projects even when `Package registry` is turned off

An issue has been discovered in GitLab affecting all versions starting from 13.2 before 16.4.3, all versions starting from 16.5 before 16.5.3, all versions starting from 16.6 before 16.6.1. It was possible for users to access composer packages on public projects that have package registry disabled in the project settings.

This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N`, 4.3).
It is now mitigated in the latest release and is assigned [CVE-2023-3964](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3964).

Thanks [js_noob](https://hackerone.com/js_noob) for reporting this vulnerability through our HackerOne bug bounty program.

### Unauthorized member can gain `Allowed to push and merge` access and affect integrity of protected branches

An issue has been discovered in GitLab EE affecting all versions starting from 8.13 before 16.4.3, all versions starting from 16.5 before 16.5.3, all versions starting from 16.6 before 16.6.1. It was possible for an attacker to abuse the `Allowed to merge` permission as a guest user, when granted the permission through a group.

This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:L/A:N`, 3.1).
It is now mitigated in the latest release and is assigned [CVE-2023-4658](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4658).

Thanks [theluci](https://hackerone.com/theluci) for reporting this vulnerability through our HackerOne bug bounty program.

### Guest users can react (emojis) on confidential work items which they cant see in a project

An issue has been discovered in GitLab affecting all versions starting from 12.1 before 16.4.3, all versions starting from 16.5 before 16.5.3, all versions starting from 16.6 before 16.6.1. It was possible for a Guest user to add an emoji on confidential work items.

This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:L/A:N`, 3.1).
It is now mitigated in the latest release and is assigned [CVE-2023-3443](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3443).

Thanks [ashish_r_padelkar](https://hackerone.com/ashish_r_padelkar) for reporting this vulnerability through our HackerOne bug bounty program.

### Mattermost Security Update

Mattermost has been updated to the latest patch release to mitigate several security issues.

### Update to PG 14.9 and 13.12

PostgreSQL has been updated to 14.9 and 13.12 to mitigate CVE-2023-39417.

### Update pcre2 to 10.42

`pcre2` has been updated to version 10.42 to mitigate CVE-2022-41409.

## Non Security Patches

### 16.6.1

* [Install Gitaly dependencies for project archiving (16.6 backport)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1639)
* [Fix intermittent 404 errors loading GitLab Pages](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137167)
* [Prefer custom sort order with search in users API](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136888)
* [Backport "Fix group page erroring because of nil user" to 16-6-stable-ee](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136902)
* [Skip encrypted settings logic for Redis when used by Mailroom](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137296)
* [Allow `+` char in abuse detection for global search](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137687)
* [Backport "Move unlock pipeline cron scheduler out of ee" to 16.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137800)
* [Fix bug with pages_deployments files not being deleted on disk](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137899)
* [Backport - Truncate verification failure message to 255](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137711)
* [Backport "Revert "Merge branch 'sc1-release-goredis' into 'master'""](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138131)

### 16.5.3

* [Backport  10871d71b171db38701bfefe15883b05c234ca6d to `16-5-stable`](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1636)
* [Geo: Reduce batch size of verification state backfill](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136934)

### 16.4.3

* [Backport  10871d71b171db38701bfefe15883b05c234ca6d to `16-4-stable`](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1622)
* [Backport to 16.4 the fix for test failure due to "not-existing.com" being registered](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6554)
* [Bump `asdf-bootstrapped-verify` version on 16.4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135647)
* [Fix bulk batch export of badges and uploads](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/133886)
* [[16.4] ci: Fix broken master by not reading GITLAB_ENV](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136764)
* [Fix assign security check permission checks](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136433)
* [For 16.4: Fix Geo verification state backfill job can exceed batch size](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136774)
* [Geo: Reduce batch size of verification state backfill](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136937)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Security Release Notifications

To receive security release blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [security release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).
